/***** Object:  StoredProcedure [dbo].[SpdPostsUpdate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Update Posts
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostsUpdate]

	@Id INT,
	@Fk_id_category INT,
	@Fk_id_administrator INT,
	@Title VARCHAR(50),
	@Contents VARCHAR(MAX),
	@Rate VARCHAR(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	UPDATE Posts  SET
		Fk_id_category = @Fk_id_category ,
		Fk_id_administrator = @Fk_id_administrator ,
		Title=@Title ,
		Contents=@Contents ,
		Rate=@Rate
	WHERE Id = @Id



END
GO
/***** Object:  StoredProcedure [dbo].[SpdPostsCreate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Add a new post
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostsCreate]

	@Id INT,
	@Fk_id_category INT,
	@Fk_id_administrator INT,
	@Title VARCHAR(50),
	@Contents VARCHAR(MAX),
	@Rate VARCHAR(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	INSERT INTO Posts (Id, Fk_id_category, Fk_id_administrator, Title, Contents, Rate)
	VALUES (@Id, @Fk_id_category, @Fk_id_administrator, @Title, @Contents, @Rate)

	SELECT SCOPE_IDENTITY()

END
GO
/***** Object:  StoredProcedure [dbo].[SpdPostsGetAll]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Get all the posts
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostsGetAll]

	@Id INT,
	@Fk_id_category INT,
	@Fk_id_administrator INT,
	@Title VARCHAR(50),
	@Contents VARCHAR(MAX),
	@Rate VARCHAR(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT *
	FROM Posts

END
GO
/***** Object:  StoredProcedure [dbo].[SpdPostGetById]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Carga el cliente por id
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostGetById]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT * FROM Posts WHERE Id = @Id


END
GO
/***** Object:  StoredProcedure [dbo].[SpdClienteEliminar]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description: Delete post
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostsDelete]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	DELETE Posts WHERE Id = @Id


END
GO