/***** Object:  StoredProcedure [dbo].[SpdCategoriesUpdate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Update Categories
-- =============================================
CREATE PROCEDURE [dbo].[SpdCategoriesUpdate]

	@Id INT,
	@Name VARCHAR(50),
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	UPDATE Categories  SET
		Name = @Name
	WHERE Id = @Id



END
GO
/***** Object:  StoredProcedure [dbo].[SpdCategoriesCreate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Add a new post
-- =============================================
CREATE PROCEDURE [dbo].[SpdCategoriesCreate]

	@Id INT,
	@Name VARCHAR(50)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	INSERT INTO Categories (Id, Name)
	VALUES (@Id, @Name)

	SELECT SCOPE_IDENTITY()

END
GO
/***** Object:  StoredProcedure [dbo].[SpdCategoriesGetAll]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Get all the Categories
-- =============================================
CREATE PROCEDURE [dbo].[SpdCategoriesGetAll]

	@Id INT,
	@Name VARCHAR(50)


AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT *
	FROM Categories

END
GO
/***** Object:  StoredProcedure [dbo].[SpdCategoriesGetById]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Carga el cliente por id
-- =============================================
CREATE PROCEDURE [dbo].[SpdCategoriesGetById]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT * FROM Categories WHERE Id = @Id


END
GO
/***** Object:  StoredProcedure [dbo].[SpdClienteEliminar]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description: Delete post
-- =============================================
CREATE PROCEDURE [dbo].[SpdCategoriesDelete]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	DELETE Categories WHERE Id = @Id


END
GO