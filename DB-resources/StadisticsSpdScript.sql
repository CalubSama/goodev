/***** Object:  StoredProcedure [dbo].[SpdStadisticsUpdate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Update Stadistics
-- =============================================
CREATE PROCEDURE [dbo].[SpdStadisticsUpdate]
	@Id Int,
	@Hours VARCHAR,
	@RightClicks INT,
	@Sheared INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	UPDATE Stadistics  SET
		Hours = @Hours,
		RightClicks = @RightClicks,
		Sheared = @Sheared
	WHERE Id = @Id



END
GO
/***** Object:  StoredProcedure [dbo].[SpdStadisticsCreate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Add a new Stadistics
-- =============================================
CREATE PROCEDURE [dbo].[SpdStadisticsCreate]

	@Id INT,
	@Hours VARCHAR,
	@RightClicks INT,
	@Sheared INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	INSERT INTO Stadistics (Id, Hours, RightClicks, Sheared)
	VALUES (@Id, @Hours, @RightClicks, @Sheared)

	SELECT SCOPE_IDENTITY()

END
GO
/***** Object:  StoredProcedure [dbo].[SpdStadisticsGetAll]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Get all the Stadistics
-- =============================================
CREATE PROCEDURE [dbo].[SpdStadisticsGetAll]

	@Id INT,
	@Hours VARCHAR,
	@RightClicks INT,
	@Sheared INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT *
	FROM Stadistics

END
GO
/***** Object:  StoredProcedure [dbo].[SpdStadisticsGetById]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Carga el cliente por id
-- =============================================
CREATE PROCEDURE [dbo].[SpdStadisticsGetById]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT * FROM Stadistics WHERE Id = @Id


END
GO
/***** Object:  StoredProcedure [dbo].[SpdClienteEliminar]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description: Delete Stadistics
-- =============================================
CREATE PROCEDURE [dbo].[SpdStadisticsDelete]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	DELETE Stadistics WHERE Id = @Id


END
GO