/***** Object:  StoredProcedure [dbo].[SpdAdministratorsUpdate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Update Administrators
-- =============================================
CREATE PROCEDURE [dbo].[SpdAdministratorsUpdate]

	@Id INT,
	@Name VARCHAR(20),
	@Password VARCHAR(10),
	@Email VARCHAR(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	UPDATE Administrators  SET
		Name=@Name ,
		Password=@Password ,
		Email=@Email
	WHERE Id = @Id



END
GO
/***** Object:  StoredProcedure [dbo].[SpdAdministratorsCreate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Add a new post
-- =============================================
CREATE PROCEDURE [dbo].[SpdAdministratorsCreate]

	@Id INT,
	@Name VARCHAR(20),
	@Password VARCHAR(10),
	@Email VARCHAR(MAX)

AS
BEGIN
	INSERT INTO Administrators (Id, Name, Password, Email)
	VALUES (@Id,@Name, Password, @Email )
	-- SET NOCOUNT ON added to prevent extra result sets from

	SELECT SCOPE_IDENTITY()

END
GO
/***** Object:  StoredProcedure [dbo].[SpdAdministratorsGetAll]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Get all the Administrators
-- =============================================
CREATE PROCEDURE [dbo].[SpdAdministratorsGetAll]

	@Id INT,
	@Name VARCHAR(20),
	@Password VARCHAR(10),
	@Email VARCHAR(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT *
	FROM Administrators

END
GO
/***** Object:  StoredProcedure [dbo].[SpdPostGetById]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Carga el cliente por id
-- =============================================
CREATE PROCEDURE [dbo].[SpdPostGetById]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT * FROM Administrators WHERE Id = @Id


END
GO
/***** Object:  StoredProcedure [dbo].[SpdClienteEliminar]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description: Delete post
-- =============================================
CREATE PROCEDURE [dbo].[SpdAdministratorsDelete]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	DELETE Administrators WHERE Id = @Id


END
GO