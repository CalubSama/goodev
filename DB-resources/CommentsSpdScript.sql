/***** Object:  StoredProcedure [dbo].[SpdCommentsUpdate]    Script Date: 08/03/2021 10:35:39 a. m. *****/

SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Update Comments
-- =============================================
CREATE PROCEDURE [dbo].[SpdCommentsUpdate]

	@Id INT,
	@Fk_id_visitor INT,
	@Message VARCHAR(125),
	@Rate VARCHAR(2)

AS
BEGIN
	-- NOCOUNT ON added to prevent extra result sets from
	UPDATE Comments  SET
		Fk_id_visitor = @Fk_id_visitor ,
		Message=@Message ,
		Rate=@Rate
	WHERE Id= @Id



END
GO
/***** Object:  StoredProcedure [dbo].[SpdCommentsCreate]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Add a new Comments
-- =============================================
CREATE PROCEDURE [dbo].[SpdCommentsCreate]

	@Id INT,
	@Fk_id_visitor INT,
	@Message VARCHAR(125),
	@Rate VARCHAR(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	INSERT INTO Comments (Id, Fk_id_visitor, Message, Rate)
	VALUES (@Id, @Fk_id_visitor,@Message,@Rate)

	SELECT SCOPE_IDENTITY()

END
GO
/***** Object:  StoredProcedure [dbo].[SpdCommentsGetAll]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Get all the Comments
-- =============================================
CREATE PROCEDURE [dbo].[SpdCommentsGetAll]

	@Id INT,
	@Fk_id_visitor INT,
	@Message VARCHAR(125),
	@Rate VARCHAR(2)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT *
	FROM Comments

END
GO
/***** Object:  StoredProcedure [dbo].[SpdCommentsGetById]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description:	Carga el cliente por id
-- =============================================
CREATE PROCEDURE [dbo].[SpdCommentsGetById]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	SELECT * FROM Comments WHERE Id = @Id


END
GO
/***** Object:  StoredProcedure [dbo].[SpdClienteEliminar]    Script Date: 08/03/2021 10:35:39 a. m. *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caleb
-- Create date: 2021-03-09
-- Description: Delete Comments
-- =============================================
CREATE PROCEDURE [dbo].[SpdCommentsDelete]

	@Id INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	DELETE Comments WHERE Id = @Id


END
GO