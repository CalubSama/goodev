import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { JwtHelperService } from "@auth0/angular-jwt";
import { UsuarioService } from '../services/usuario.service';
import { promise } from 'protractor';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,
    private usuarioService: UsuarioService) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {



      // return new Promise((resolve, reject) => {
      //   this.usuarioService.validar(state.url).subscribe(
      //     (data: boolean) => {

      //       if(!data) {
      //         localStorage.removeItem('token');
      //         this.router.navigate(['login']);
      //       }

      //       resolve(data);
      //     },
      //     (error)=>{
      //       reject(error)
      //     });
      // });

    const token = localStorage.getItem('token');
    const helper = new JwtHelperService();
    if (token && !helper.isTokenExpired(token)) {
      return true;
    }


  }
}
