import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { JwtInterceptor } from './helpers/jwtInterceptor';
import { SharedModule } from './modules/shared/shared.module';
import { AdministratorComponent } from './pages/administrators/administrators.component';
import { NavbarComponent } from './modules/public/components/navbar/navbar.component';
import { CategoryComponent } from './modules/public/components/category/category.component';
import { CategoryPageComponent } from './modules/public/pages/category/category.component';
import { ContactComponent } from './modules/public/components/contact/contact.component';
import { PageContactComponent } from './modules/public/pages/contact/contact.component';
import { PublicFooterComponent } from './modules/public/components/footer/footer.component';
import { MainComponent } from './modules/public/components/main/main.component';
import { PostComponent } from './modules/public/components/post/post.component';
import { PagePostComponent } from './modules/public/pages/post/post.component';
import { RegisterComponent } from './modules/public/components/register/register.component';
import { PageRegisterComponent } from './modules/public/pages/register/register.component';
import { IndexComponent } from './modules/public/pages/index/index.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdministratorComponent,
    NavbarComponent,
    CategoryComponent,
    CategoryPageComponent,
    ContactComponent,
    PublicFooterComponent,
    MainComponent,
    PostComponent,
    PagePostComponent,
    RegisterComponent,
    PageRegisterComponent,
    IndexComponent,
    PageContactComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    RouterModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
