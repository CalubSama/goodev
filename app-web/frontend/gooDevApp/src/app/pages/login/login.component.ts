import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: Usuario = new Usuario();
  error = '';
  constructor(
    private usuarioService: UsuarioService,
    private router: Router) { }

  ngOnInit() {
  }


  login() {
    this.error = '';

    this.usuarioService.login(this.usuario).subscribe(
      (data: any) => {

        if (data.idUsuario > 0) {

          localStorage.setItem('token', data.token);

          // Redirecciona
          this.router.navigate(['catalogos/inicio']);
        } else {

          this.error = data.error;
        }

      },
      (error) => {

      }
    );

  }

}
