import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserComponent } from './pages/user/user.component';
import { LayoutAccountComponent } from './layout-account/layout-account.component';
import { RouterModule } from '@angular/router';
import { AccountRoutingModule } from './account.routing';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [UserComponent, LayoutAccountComponent],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    AccountRoutingModule,
    FormsModule
  ]
})
export class AccountModule { }
