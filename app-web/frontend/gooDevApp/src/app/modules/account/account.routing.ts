import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LayoutAccountComponent } from './layout-account/layout-account.component';
import { UserComponent } from './pages/user/user.component';
export const routes: Routes = [
  {
    path: '',
    component: LayoutAccountComponent,
    children: [
      { path: 'user', component: UserComponent },
    ]
  }
]

//export const routingCatalogos: ModuleWithProviders = RouterModule.forChild(childRoutes  );

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}
