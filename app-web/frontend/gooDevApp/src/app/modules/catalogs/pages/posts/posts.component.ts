import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  //vars
  posts:Array<Post>;
  post = new Post();

  constructor(
    private postService:PostService,
    private modalService: NgbModal) { }

  ngOnInit() {

    this.postService.getAll().subscribe((posts:Array<Post>)=> {

      this.posts= posts;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });
  }

  nuevo(modal){

    this.modalService.open(modal, {size: 'lg'}).result.then((result) => {

    }, (reason) => {

    });
  }

  agregar() {
    this.postService.add(this.post).subscribe(()=> {
    }, error=> {
      console.error("Add Error >>>", error);
    });
  }

  editar(post: Post, modal) {
    this.post= post;
    this.nuevo(modal);
  }
  
  delete(postId: number){
    this.postService.delete(postId).subscribe(()=>{ 
    }, error=>{
      console.error("Delete Error >>>", error);
    });
  }

}
