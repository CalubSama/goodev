import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Stadistic } from '../../../../models/stadistic';
import { StadisticService } from 'src/app/services/stadistics.service';

@Component({
  selector: 'app-stadistics',
  templateUrl: './stadistics.component.html',
  styleUrls: ['./stadistics.component.css']
})
export class StadisticsComponent implements OnInit {
  //vars
  stadistics:Array<Stadistic>;
  stadistic = new Stadistic();

  constructor( private stadisticService: StadisticService, private modalService: NgbModal) { }

  ngOnInit() {
    this.stadisticService.getAll().subscribe((stadistics:Array<Stadistic>)=> {

      this.stadistics= stadistics;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });
  }

/**
 * 
 * @param modal item to render
 * @return modal component
 */
 nuevo(modal){

  this.modalService.open(modal, {size: 'lg'}).result.then((result) => {

  }, (reason) => {

  });
}

/**
 * add category method with the object category
 */
add() {
  if (this.stadistic.id != 0){
    this.update(this.stadistic);
  }else{
    this.stadisticService.add(this.stadistic).subscribe(()=> {
    }, error=> {
      console.error("Add Error >>>", error);
    });
  }
}
/**
* get the obj in modal
* @param modal modal 
* @param stadistic stadistic obj
*/
editar(stadistic: Stadistic, modal) {
  this.stadistic=stadistic;
  this.nuevo(modal);
}

/**
* @param stadistic stadistic id
*/
delete(stadisticId: number){
  this.stadisticService.delete(stadisticId).subscribe(()=>{ 
  }, error=>{
    console.error("Delete Error >>>", error);
  });
}

/**
* get the obj in modal
* @param modal modal 
* @param stadistic stadistic obj
*/
update(stadistic: Stadistic){
  this.stadisticService.update(stadistic).subscribe(()=>{
  },error=>{
    console.error("Update Error >>>", error);
  })
}


}
