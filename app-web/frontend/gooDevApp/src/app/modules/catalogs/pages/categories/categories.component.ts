import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Category } from '../../../../models/category';
import { CategoryService } from 'src/app/services/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  //vars
  categories:Array<Category>;
  category = new Category();

  constructor( private categoryService: CategoryService,private modalService: NgbModal){}
  

  ngOnInit() {
    this.categoryService.getAll().subscribe((categories:Array<Category>)=> {

      this.categories= categories;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });
  }

/**
 * 
 * @param modal item to render
 * @return modal component
 */
  nuevo(modal){

    this.modalService.open(modal, {size: 'lg'}).result.then((result) => {

    }, (reason) => {

    });
  }

  /**
   * add category method with the object category
   */
  agregar() {
    this.categoryService.add(this.category).subscribe(()=> {
    }, error=> {
      console.error("Add Error >>>", error);
    });
  }
/**
 * get the obj in modal
 * @param modal modal 
 * @param Category category obj
 */
  editar(category: Category, modal) {
    this.category= category;
    this.nuevo(modal);
  }

  delete(categoryId: number){
    this.categoryService.delete(categoryId).subscribe(()=>{ 
    }, error=>{
      console.error("Delete Error >>>", error);
    });
  }
  upload(category: Category){
    this.categoryService.update(category).subscribe(()=>{
    },error=>{
      console.error("Update Error >>>", error);
    })
  }

}
