import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LayoutCatalogosComponent } from './layout-catalogs/layout-catalogs.component';
import { HomeComponent } from './pages/home/home.component';
import { PostsComponent } from './pages/posts/posts.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { StadisticsComponent } from './pages/stadistics/stadistics.component';

export const routes: Routes = [
  {
    path: '',
    component: LayoutCatalogosComponent,
    children: [
      { path: 'posts', component: PostsComponent },
      { path: 'inicio', component: HomeComponent },
      { path: 'categories', component: CategoriesComponent },
      { path: 'stadistics', component: StadisticsComponent }
    ]
  }
]

//export const routingCatalogos: ModuleWithProviders = RouterModule.forChild(childRoutes  );

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CatalogosRoutingModule {}
