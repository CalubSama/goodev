import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutCatalogosComponent } from './layout-catalogs/layout-catalogs.component';
import {  CatalogosRoutingModule } from './catalogos.routing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { RmmComponentsModule } from 'rmm-components';
import { PostsComponent } from './pages/posts/posts.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { StadisticsComponent } from './pages/stadistics/stadistics.component';




@NgModule({
  declarations: [
    LayoutCatalogosComponent,
    HomeComponent,
    PostsComponent,
    CategoriesComponent,
    StadisticsComponent
  ],
  imports: [
     CommonModule,
     FormsModule,
     RouterModule,
    SharedModule,
    CatalogosRoutingModule,
    RmmComponentsModule
  ]
})
export class CatalogosModule { }
