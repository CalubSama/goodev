import { Component, OnInit } from '@angular/core';
import { Category } from '../../../../models/category';
import { CategoryService } from 'src/app/services/category.service';
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories:Array<Category>;
  category = new Category();

  constructor(private categoryService: CategoryService,) { }

  ngOnInit() {
    this.categoryService.getAll().subscribe((categories:Array<Category>)=> {

      this.categories= categories;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });
  }
  

}
