import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post';
import { PostService } from 'src/app/services/post.service';
import { Category } from '../../../../models/category';
import { CategoryService } from 'src/app/services/category.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  categories:Array<Category>;
  category = new Category();
  posts:Array<Post>;
  post = new Post();
  imgPath = "https://picsum.photos/id/1/530/530";

  constructor( private postService:PostService , private categoryService: CategoryService,) { }

  ngOnInit() {
        
    this.postService.getAll().subscribe((posts:Array<Post>)=> {

      this.posts= posts;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });

    this.categoryService.getAll().subscribe((categories:Array<Category>)=> {

      this.categories= categories;

    }, error=> {
      console.error("GetAll Error >>>", error);
    });
  }

}
