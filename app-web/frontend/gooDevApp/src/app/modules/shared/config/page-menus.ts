var pageMenus = [{
  'icon': 'fa fa-th-large',
  'title': 'Posts',
  'url': '/catalogs/posts',
},
{
  'icon': 'fa fa-th-large',
  'title': 'Categories',
  'url': '/catalogs/categories',
},
{
  'icon': 'fa fa-th-large',
  'title': 'Stadistics',
  'url': '/catalogs/stadistics',
  'submenu': [{
    'url': '/stadistics/months',
    'title': 'Months',
  },
  {
    'url': '/stadistics/days',
    'title': 'Days',
  }]
},
{
  'icon': 'fa fa-user',
  'title': 'Account',
  'url': '',
  'caret': 'true',
  'submenu': [{
    'url': '/account/user',
    'title': 'Profile',

  }
]
}];

export default pageMenus;
