import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Category } from '../models/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }

  url = `${environment.rutaAPICategory}category/`;


  getAll():Observable<Array<Category>>
  {

    return this.httpClient.get<Array<Category>>(this.url+'getall');
  }

  getById(idCategory: number)
  {

    return this.httpClient.get<Category>(this.url+'getById?id='+idCategory);
  }

  add(category: Category)
  {

    return this.httpClient.post(this.url+'add',category);
  }

  update(category:Category)
  {

    return this.httpClient.post(this.url+'update',category);
  }

  delete(id: number)
  {

    return this.httpClient.post(this.url+'delete', {id: id});
  }

}
