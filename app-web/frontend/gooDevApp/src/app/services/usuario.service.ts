import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url = environment.rutaAPi+ 'login/';
  constructor(private httpClient: HttpClient) { }


  login(usuario: Usuario) {

    const url = `${this.url}`;
    return this.httpClient.post(url, usuario);

  }

 
}
