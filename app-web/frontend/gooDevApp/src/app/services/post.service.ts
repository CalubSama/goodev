import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Post } from '../models/post';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private httpClient: HttpClient) { }

  url = `${environment.rutaAPi}post/`;


  getAll():Observable<Array<Post>>
  {

    return this.httpClient.get<Array<Post>>(this.url+'getall');
  }

  getById(idPost: number)
  {

    return this.httpClient.get<Post>(this.url+'getById?id='+idPost);
  }

  add(post:Post)
  {

    return this.httpClient.post(this.url+"Add",post);
  }

  update(post:Post)
  {

    return this.httpClient.post(this.url+'update',post);
  }

  delete(id: number)
  {

    return this.httpClient.post(this.url+'delete', {id: id});
  }

}
