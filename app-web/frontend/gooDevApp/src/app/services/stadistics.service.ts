import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Stadistic } from '../models/stadistic';

@Injectable({
  providedIn: 'root'
})
export class StadisticService {

  constructor(private httpClient: HttpClient) { }

  url = `${environment.rutaAPIStadistic}stadistics/`;


  getAll():Observable<Array<Stadistic>>
  {

    return this.httpClient.get<Array<Stadistic>>(this.url+'getall');
  }

  getById(idStadistic: number)
  {

    return this.httpClient.get<Stadistic>(this.url+'getById?id='+idStadistic);
  }

  add(stadistic:Stadistic)
  {

    return this.httpClient.post(this.url+"Add",stadistic);
  }

  update(stadistic:Stadistic)
  {

    return this.httpClient.post(this.url+'update',stadistic);
  }

  delete(id: number)
  {

    return this.httpClient.post(this.url+'delete', {id: id});
  }

}
