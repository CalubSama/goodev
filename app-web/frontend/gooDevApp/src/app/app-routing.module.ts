import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './modules/public/pages/index/index.component';
import { AdministratorComponent } from './pages/administrators/administrators.component';
import { LoginComponent } from './pages/login/login.component';
import { PagePostComponent } from './modules/public/pages/post/post.component';
import { CategoryPageComponent } from './modules/public/pages/category/category.component';
import { PageContactComponent } from './modules/public/pages/contact/contact.component';
import { PageRegisterComponent } from './modules/public/pages/register/register.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'index', component: IndexComponent },
  { path: 'post', component: PagePostComponent },
  { path: 'category', component: CategoryPageComponent },
  { path: 'contact', component: PageContactComponent },
  { path: 'register', component: PageRegisterComponent },
 { path: 'catalogs', loadChildren: './modules/catalogs/catalogos.module#CatalogosModule' },
  { path: 'account', loadChildren: './modules/account/account.module#AccountModule' },



];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
