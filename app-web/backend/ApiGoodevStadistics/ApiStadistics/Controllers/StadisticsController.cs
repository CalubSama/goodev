﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LBusStadistics;
using LModStadistics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiStadistics.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StadisticsController : ControllerBase
    {
        [HttpGet("GetAll")]
        public IActionResult GetAll(int id, string name)
        {

            try
            {
                CBusStadistic objBusStadistic = new CBusStadistic();
                var result = objBusStadistic.GetAll(new LModStadistics.CModStadistic());
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

            }

        [HttpGet("GetById")]
        public IActionResult GetById(int id)
        {

            try
            {
                CBusStadistic objBusStadistic = new CBusStadistic();
                var result = objBusStadistic.GetById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }


        [HttpPost("Add")]
        public IActionResult Add(CModStadistic objModStadistic)
        {

            try
            {
                CBusStadistic objBusStadistic = new CBusStadistic();
                var result = objBusStadistic.Add(objModStadistic);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        [HttpPut("Update")]
        public IActionResult Update(CModStadistic objModStadistic)
        {

            try
            {
                CBusStadistic objBusStadistic = new CBusStadistic();
                var result = objBusStadistic.Update(objModStadistic);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        [HttpPost("Delete")]
        public IActionResult Delete(CModStadistic objModStadistic)
        {

            try
            {
                CBusStadistic objBusStadistic = new CBusStadistic();
                var result = objBusStadistic.Delete(objModStadistic);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }
    }
}