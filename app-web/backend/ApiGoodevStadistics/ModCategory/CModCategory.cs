﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModCategories
{
    public class CModCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
