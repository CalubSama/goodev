﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using ModCategories;

namespace DatCategories
{
    public class CDatCategory
    {
        //Connection String of my DB
        private const string _Conexion = "Server=localhost;Database=goodev;Trusted_Connection=True;";

        /// <summary>
        /// Get All
        /// </summary>
        /// <param name="objModCategory"></param>
        /// <returns></returns>
        public IEnumerable<CModCategory> GetAll(CModCategory objModCategory)
        {

            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModCategory>("SpdCategoriesGetAll", new
                    {
                        Id = objModCategory.Id,
                        Name = objModCategory.Name
                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error CDatCategory, " + ex.Message);

            }
        }

        /// <summary>
        /// Add category
        /// </summary>
        /// <param name="objModCategory"></param>
        /// <returns></returns>
        public int Add(CModCategory objModCategory)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.ExecuteScalar<int>("SpdCategoriesCreate", new
                    {
                        Name = objModCategory.Name
                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error CDatCategory, " + ex.Message);

            }
        }

        /// <summary>
        /// Update Category
        /// </summary>
        /// <param name="objModCategory"></param>
        /// <returns></returns>
        public IEnumerable<CModCategory> Update(CModCategory objModCategory)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModCategory>("SpdCategoriesUpdate", new
                    {
                        Id = objModCategory.Id,
                        Name = objModCategory.Name
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error CDatCategory, " + ex.Message);
            }
        }

        /// <summary>
        /// Delete Category
        /// </summary>
        /// <param name="objModCategory"></param>
        /// <returns></returns>
        public IEnumerable<CModCategory> Delete(CModCategory objModCategory)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModCategory>("SpdCategoriesDelete", new
                    {
                        Id = objModCategory.Id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error CDatCategory, " + ex.Message);
            }
        }

        /// <summary>
        /// Get category by Id
        /// </summary>
        /// <param name="id">Id of the category to search</param>
        /// <returns></returns>
        public CModCategory GetById(int id)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.QuerySingleOrDefault<CModCategory>("SpdCategoriesGetById", new
                    {
                        Id = id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error CDatCategory, " + ex.Message);
            }
        }
    }
}

