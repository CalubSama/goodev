﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using Dapper;
using LModStadistics;

namespace LDataStadistics
{
    public class CDataStadistic
    {
        private const string _Conexion = "Server=localhost;Database=goodev;Trusted_Connection=True;";

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="objModStadistic">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public int Add(CModStadistic objModStadistic)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.ExecuteScalar<int>("SpdStadisticsCreate", new
                    {
                        RightClick= objModStadistic.RightClick,
                        Hours= objModStadistic.Hours,
                        Sheared= objModStadistic.Sheared
                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error ClsDatCatPost, " + ex.Message);

            }
        }

        /// <summary>
        /// Get ALL
        /// </summary>
        /// <param name="objModStadistic">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public IEnumerable<CModStadistic> GetAll(CModStadistic objModStadistic)
        {

            try
            {
                //as
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModStadistic>("SpdStadisticsGetAll", new
                    {
                        Id = objModStadistic.Id,
                        RightClicks = objModStadistic.RightClick,
                        Hours = objModStadistic.Hours,
                        Sheared = objModStadistic.Sheared

                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error ClsDatCatPost, " + ex.Message);

            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="objModStadistic">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public IEnumerable<CModStadistic> Update(CModStadistic objModStadistic)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModStadistic>("SpdStadisticsUpdate", new
                    {
                        Id = objModStadistic.Id,
                        RightClicks = objModStadistic.RightClick,
                        Hours = objModStadistic.Hours,
                        Sheared = objModStadistic.Sheared

                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }

        public IEnumerable<CModStadistic> Delete(CModStadistic objModStadistic)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModStadistic>("SpdStadisticsDelete", new
                    {
                        Id = objModStadistic.Id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }

        public CModStadistic GetById(int id)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.QuerySingleOrDefault<CModStadistic>("SpdStadisticsGetById", new
                    {
                        Id = id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }
    }
}
