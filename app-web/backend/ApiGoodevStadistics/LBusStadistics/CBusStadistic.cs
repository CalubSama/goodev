﻿using LDataStadistics;
using LModStadistics;
using System;
using System.Collections.Generic;

namespace LBusStadistics
{
    public class CBusStadistic
    {
        public IEnumerable<CModStadistic> GetAll(CModStadistic objModStadistic)
        {
            return new CDataStadistic().GetAll(objModStadistic);
        }

        public int Add(CModStadistic objModStadistic)
        {
            return new CDataStadistic().Add(objModStadistic);
        }

        public IEnumerable<CModStadistic> Update(CModStadistic objModStadistic)
        {
            return new CDataStadistic().Update(objModStadistic);
        }

        public IEnumerable<CModStadistic> Delete(CModStadistic objModStadistic)
        {
            return new CDataStadistic().Delete(objModStadistic);
        }

        public CModStadistic GetById(int id)
        {
            return new CDataStadistic().GetById(id);
        }
    }
}
