﻿using DatCategories;
using ModCategories;
using System;
using System.Collections.Generic;
using System.Text;

namespace NegCategories
{
   public class CNegCategory
    {
        public IEnumerable<CModCategory> GetAll(CModCategory objModPost)
        {
            return new CDatCategory().GetAll(objModPost);
        }

        public int Add(CModCategory objModPost)
        {
            return new CDatCategory().Add(objModPost);
        }

        public IEnumerable<CModCategory> Update(CModCategory objModPost)
        {
            return new CDatCategory().Update(objModPost);
        }

        public IEnumerable<CModCategory> Delete(CModCategory objModPost)
        {
            return new CDatCategory().Delete(objModPost);
        }

        public CModCategory GetById(int id)
        {
            return new CDatCategory().GetById(id);
        }

    }
}
