﻿using System;

namespace LModStadistics
{
    public class CModStadistic
    {
        public int Id { get; set; }
        public string Hours { get; set; }
        public int RightClick { get; set; }
        public int Sheared { get; set; }
    }
}
