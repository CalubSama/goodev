﻿using LModPost;
using System;
using System.Collections.Generic;
using Dapper;
using System.Data.SqlClient;

namespace LDataPost
{
    public class CDataPost
    {
         private const string _Conexion = "Server=localhost;Database=goodev;Trusted_Connection=True;";

        /// <summary>
        /// Add
        /// </summary>
        /// <param name="objModPost">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public int Add(CModPost objModPost)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.ExecuteScalar<int>("SpdPostsCreate", new
                    {
                        Fk_id_category = objModPost.idCategory,
                        Fk_id_administrator = objModPost.idAdministrator,
                        Title = objModPost.title,
                        Rate = objModPost.rate,
                        Contents = objModPost.contents
                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error ClsDatCatPost, " + ex.Message);

            }
        }

        /// <summary>
        /// Get ALL
        /// </summary>
        /// <param name="objModPost">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public IEnumerable<CModPost> GetAll(CModPost objModPost)
        {

            try
            {
                //as
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModPost>("SpdPostsGetAll", new
                    {
                        Id = objModPost.Id,
                        Fk_id_category = objModPost.idCategory,
                        Fk_id_administrator = objModPost.idAdministrator,
                        Title = objModPost.title,
                        Rate = objModPost.rate,
                        Contents = objModPost.contents
                    }, commandType: System.Data.CommandType.StoredProcedure);

                }


            }
            catch (Exception ex)
            {

                throw new Exception("Error ClsDatCatPost, " + ex.Message);

            }
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="objModPost">Objet with the fields</param>
        /// <returns>ObjResult</returns>
        public IEnumerable<CModPost> Update(CModPost objModPost)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModPost>("SpdPostsUpdate", new
                    {
                        Id = objModPost.Id,
                        Fk_id_category = objModPost.idCategory,
                        Fk_id_administrator = objModPost.idAdministrator,
                        Title = objModPost.title,
                        Rate = objModPost.rate,
                        Contents = objModPost.contents
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }

        public IEnumerable<CModPost> Delete(CModPost objModPost)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.Query<CModPost>("SpdPostsDelete", new
                    {
                        Id = objModPost.Id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }

        public CModPost GetById(int id)
        {
            try
            {
                using (var coneccion = new SqlConnection(_Conexion))
                {
                    return coneccion.QuerySingleOrDefault<CModPost>("SpdPostsGetById", new
                    {
                        Id = id
                    }, commandType: System.Data.CommandType.StoredProcedure);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error ClsDatCatPost, " + ex.Message);
            }
        }
    }
}
