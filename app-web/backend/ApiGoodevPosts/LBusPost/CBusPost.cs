﻿using LDataPost;
using LModPost;
using System;
using System.Collections.Generic;

namespace LBusPost
{
    public class CBusPost
    {
        public IEnumerable<CModPost> GetAll(CModPost objModPost)
        {
            return new CDataPost().GetAll(objModPost);
        }

        public int Add(CModPost objModPost)
        {
            return new CDataPost().Add(objModPost);
        }

        public IEnumerable<CModPost> Update(CModPost objModPost)
        {
            return new CDataPost().Update(objModPost);
        }

        public IEnumerable<CModPost> Delete(CModPost objModPost)
        {
            return new CDataPost().Delete(objModPost);
        }

        public CModPost GetById(int id)
        {
            return new CDataPost().GetById(id);
        }

    }
}
