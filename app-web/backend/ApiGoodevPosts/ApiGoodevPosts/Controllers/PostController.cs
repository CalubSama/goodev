﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LBusPost;
using LModPost;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiGoodevPosts.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostController : ControllerBase
    {
        [HttpGet("GetAll")]
        public IActionResult GetAll(string codigo, string nombre)
        {

            try
            {
                CBusPost objBusPost = new CBusPost();
                CBusPost objPost = new CBusPost();
                var result = objBusPost.GetAll(new LModPost.CModPost());
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }



        public IActionResult GetById(int id)
        {

            try
            {
                CBusPost objBusPost = new CBusPost();
                var result = objBusPost.GetById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        /// <summary>
        /// HTTP Request To ADD
        /// </summary>
        /// <param name="objModPost"> Params for the obj</param>
        /// <returns></returns>
        [HttpPost("Add")]
        public IActionResult Add(CModPost objModPost)
        {

            try
            {
                CBusPost objNegPost = new CBusPost();
                var result = objNegPost.Add(objModPost);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        /// <summary>
        /// HTTP Request To Update
        /// </summary>
        /// <param name="objModPost"> Params for the obj</param>
        /// <returns></returns>
        [HttpPut("Update")]
        public IActionResult Update(CModPost objModPost)
        {

            try
            {
                CBusPost objBusPost = new CBusPost();
                var result = objBusPost.Update(objModPost);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        /// <summary>
        /// HTTP Request To ADD
        /// </summary>
        /// <param name="objModPost"> Params for the obj</param>
        /// <returns></returns>
        [HttpPost("Delete")]
        public IActionResult Delete(CModPost objModPost)
        {

            try
            {
                CBusPost objBusPost = new CBusPost();
                var result = objBusPost.Delete(objModPost);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }
    }
}