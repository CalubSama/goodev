﻿using System;

namespace LModPost
{
    public class CModPost
    {
        //Attributes of my class
        public int Id { get; set; }
        public int idCategory { get; set; }
        public int idAdministrator { get; set; }
        public string title { get; set; }
        public string rate { get; set; }
        public string contents { get; set; }
    }
}
