﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ModCategories;
using NegCategories;

namespace ApiCategories.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        [HttpGet("GetAll")]
        public IActionResult GetAll(int id, string name)
        {

            try
            {
                CNegCategory objNegCategory = new CNegCategory();
                var result = objNegCategory.GetAll(new ModCategories.CModCategory());
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        [HttpGet("GetById")]
        public IActionResult GetById(int id)
        {

            try
            {
                CNegCategory objNegCategory = new CNegCategory();
                var result = objNegCategory.GetById(id);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }


        [HttpPost("Add")]
        public IActionResult Add(CModCategory objModCategory)
        {

            try
            {
                CNegCategory objNegCategory = new CNegCategory();
                var result = objNegCategory.Add(objModCategory);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        [HttpPut("Update")]
        public IActionResult Update(CModCategory objModCategory)
        {

            try
            {
                CNegCategory objNegCategory = new CNegCategory();
                var result = objNegCategory.Update(objModCategory);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }

        [HttpPost("Delete")]
        public IActionResult Delete(CModCategory objModCategory)
        {

            try
            {
                CNegCategory objNegCategory = new CNegCategory();
                var result = objNegCategory.Delete(objModCategory);
                return Ok(result);
            }
            catch (Exception ex)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Error, " + ex.Message);
            }

        }
    }
}