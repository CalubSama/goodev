import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Post } from '../models/post';
import { Administrator } from '../models/admin';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private httpClient : HttpClient) { }

  url = `${environment.rutaAPi}post/`;


  getAll()
  {
    return new Promise(resolve=>{
      this.httpClient.get(this.url+'getall').subscribe(data=>{
          resolve(data);
      },error=>{
        console.log(error);
      });
    });

  } 


  getById(idPost: number)
  {

    return this.httpClient.get<Post>(this.url+'getById?id='+idPost);
  }

  add(post:Post)
  {

    return this.httpClient.post(this.url+"Add",post);
  }

  update(post:Post)
  {

    return this.httpClient.post(this.url+'update',post);
  }

  delete(id: number)
  {

    return this.httpClient.post(this.url+'delete', {id: id});
  }
  Login(email:string, password:string)
  {
    return this.httpClient.post(this.url+"login",{email : email , password:password});
  }
}
