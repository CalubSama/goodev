import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Category } from '../models/Category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private httpClient: HttpClient) { }
  
  url = `${environment.rutaAPICategory}category/`;


  //Get the list of all categories
  getAll()
  {
    return new Promise(resolve=>{
      this.httpClient.get(this.url+'getall').subscribe(data=>{
          resolve(data);
      },error=>{
        console.log(error);
      });
    });

  }

  //Add new category
  add(category : Category) {
    return this.httpClient.post(this.url+'add',category);
  }

  delete(id: number)
  {

    return this.httpClient.post(this.url+'delete', {id: id});
  }
  update(category:Category)
  {

    return this.httpClient.post(this.url+'update',category);
  }


// end of class
}
