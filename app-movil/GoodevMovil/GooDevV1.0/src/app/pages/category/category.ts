import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, IonList, IonRouterOutlet, LoadingController, ModalController, ToastController, Config } from '@ionic/angular';
import { ScheduleFilterPage } from '../schedule-filter/schedule-filter';
import { ConferenceData } from '../../providers/conference-data';
import { UserData } from '../../providers/user-data';
import { CategoryService } from '../../services/category.service';
import {Category} from '../../models/category'
import { Observable, throwError } from 'rxjs';
import { ControlContainer } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
  styleUrls: ['./category.scss'],
})
export class CategoryPage implements OnInit {
  // Gets a reference to the list element
  @ViewChild('scheduleList', { static: true }) scheduleList: IonList;

  ios: boolean;
  dayIndex = 0;
  queryText = '';
  segment = 'all';
  excludeTracks: any = [];
  shownSessions: any = [];
  groups: any = [];
  confDate: string;
  showSearchbar: boolean;
  categories:any;
  category = new Category();
  disabledInput :boolean = false;
  constructor(
    public alertCtrl: AlertController,
    public confData: ConferenceData,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public router: Router,
    public routerOutlet: IonRouterOutlet,
    public toastCtrl: ToastController,
    public user: UserData,
    public config: Config,
    public toastController: ToastController,
    private httpClient: HttpClient,
    private categoryService: CategoryService
  ) { }

  ngOnInit() {
    this.getAll();
    this.updateSchedule();

    this.ios = this.config.get('mode') === 'ios';
  }

 //My methods "Goodev"
  getAll() { 
    this.categoryService.getAll()
    .then(data => {
      this.categories = data;
    }, error=>{
      console.error("Get All Error");
    });
  }

  //Add a new category
  add(){
    if(this.category.id !=0){
      this.update(this.category);
    }
    this.categoryService.add(this.category).subscribe(()=>{
      this.presentToastOk();
    }, error=>{
      this.presentToastFail();
      console.error("Add error>>>");
    })
    this.ngOnInit();
    this.ngOnInit();
  }

//Delete a category by id
  delete(id:number){

      this.categoryService.delete(id).subscribe(()=>{
        this.presentToastOkDelete();
      }, error =>{
        this.presentToastFail();
        console.error("delete error");
      });
      this.ngOnInit();
     
    }
  
    modify(){
      if (this.disabledInput == false) { this.disabledInput = true;}
      else{this.disabledInput=false;} 
    }
    /**
* get the obj in modal
* @param modal modal 
* @param category category obj
*/
update(category: Category){
  this.categoryService.update(category).subscribe(()=>{
  },error=>{
    console.error("Update Error >>>", error);
  })
}

  


  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
};

  //Update on search a category  
  updateSchedule() {
    // Close any open sliding items when the schedule updates
    if (this.scheduleList) {
      this.scheduleList.closeSlidingItems();
    }

    this.confData.getTimeline(this.dayIndex, this.queryText, this.excludeTracks, this.segment).subscribe((data: any) => {
      this.shownSessions = data.shownSessions;
      this.groups = data.groups;
    });
  }

  async presentFilter() {
    const modal = await this.modalCtrl.create({
      component: ScheduleFilterPage,
      swipeToClose: true,
      presentingElement: this.routerOutlet.nativeEl,
      componentProps: { excludedTracks: this.excludeTracks }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.excludeTracks = data;
      this.updateSchedule();
    }
  }
//OK Toast
async presentToastOk() {
  const toast = await this.toastController.create({
    message: 'Your Category have been Created.',
    duration: 5000
  });
  toast.present();
}
//OK DeleteToast
async presentToastOkDelete() {
  const toast = await this.toastController.create({
    message: 'Your Category have been Removed.',
    duration: 5000
  });
  toast.present();
}
//Fail Toast
async presentToastFail() {
  const toast = await this.toastController.create({
    message: 'We have a error, check your fields.',
    duration: 2000
  });
  toast.present();
}
}
