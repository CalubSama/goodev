import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { ConferenceData } from '../../providers/conference-data';
import { PostsService } from '../../services/posts.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'page-post-list',
  templateUrl: 'post-list.html',
  styleUrls: ['./post-list.scss'],
})
export class PostListPage {
  
    posts:any;
    post = new Post();
    @Input() banModify: boolean = false;
    @Input() title:string;
    @Input() category:string;
    @Input() admin:number;
    @Input() content:string;
    @Input() rate:string;
    @Input() id:number;



  constructor(public router :Router,public confData: ConferenceData, public toastController: ToastController, private postService: PostsService) {}
  
  ionViewDidEnter() {
    this.getAll();
  }

  getAll() { 
    this.postService.getAll()
    .then(data => {
      this.posts = data;
    }, error=>{
      this.presentToastFail();
      console.error("Get All Error");
    });
  }
  delete(id:number){
    this.postService.delete(id).subscribe(()=>{
      console.log("Delete success");
      this.presentToastOk();
    },error =>{
      this.presentToastFail();
      console.error("Delete error");});
    this.getAll();
    this.ionViewDidEnter();
  }


//Toasts
//OK Toast
async presentToastOk() {
  const toast = await this.toastController.create({
    message: 'Your Post have been Deleted.',
    duration: 2000
  });
  toast.present();
}
//Fail Toast
async presentToastFail() {
  const toast = await this.toastController.create({
    message: 'We have a error, check your fields.',
    duration: 2000
  });
  toast.present();
}

modify(id:number){
  this.router.navigateByUrl('/app/tabs/posts/post-details/'+id);
  this.banModify= true;
  console.log(this.banModify);
  this.title = this.post.title;
  this.category= this.post.category;
  this.admin= this.post.idAdministrator;
  this.content= this.post.contents;
  this.rate= this.post.rate;
  this.id= this.post.id;

}

}
