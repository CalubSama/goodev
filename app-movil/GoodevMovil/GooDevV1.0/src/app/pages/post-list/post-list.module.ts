import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { PostListPage } from './post-list';
import { PostListPageRoutingModule } from './post-list-routing.module';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    PostListPageRoutingModule
  ],
  declarations: [PostListPage],
})
export class PostListModule {}
