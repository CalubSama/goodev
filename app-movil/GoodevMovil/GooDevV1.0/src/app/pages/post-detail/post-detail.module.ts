import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostDetailPage } from './post-detail';
import { PostDetailPageRoutingModule } from './post-detail-routing.module';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    PostDetailPageRoutingModule
  ],
  declarations: [
    PostDetailPage,
  ]
})
export class PostDetailModule { }
