import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConferenceData } from '../../providers/conference-data';
import { ActionSheetController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Post } from '../../models/post';
import { PostsService } from '../../services/posts.service'
import { FormsModule } from '@angular/forms';
import { CategoryService } from '../../services/category.service'
import {Category} from '../../models/category'
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import {PostListPage} from '../post-list/post-list'
@Component({
  selector: 'page-post-detail',
  templateUrl: 'post-detail.html',
  styleUrls: ['./post-detail.scss'],
})
export class PostDetailPage {
  speaker: any;
  posts:any;
  post : Post;
  categories: any;
  category: Category;
  postInput: Post = { title:"",contents: '', idCategory: 0,
   idAdministrator:0, rate:'', category:'',id:0,administrator:''};

  constructor(
    private dataProvider: ConferenceData,
    private route: ActivatedRoute,
    public actionSheetCtrl: ActionSheetController,
    public confData: ConferenceData,
    public inAppBrowser: InAppBrowser,
    private postService: PostsService,
    public toastController: ToastController,
    private categoryService: CategoryService,
    public router: Router
  ) {}


  ionViewWillEnter() {
    //get all the posts
    this.postService.getAll()
    .then(data => {
      this.posts = data;
    }, error=>{
      console.error("Get All Error");
    });
    //Get all the categories
    this.categoryService.getAll()
    .then(data => {
      this.categories = data;
    }, error=>{
      console.error("Get All Error");
    });
    
    this.dataProvider.load().subscribe((data: any) => {
      const speakerId = this.route.snapshot.paramMap.get('speakerId');
      if (data && data.speakers) {
        for (const speaker of data.speakers) {
          if (speaker && speaker.id === speakerId) {
            this.speaker = speaker;
            break;
          }
        }
      }
    });
  }

//OK Toast
  async presentToastOk() {
    const toast = await this.toastController.create({
      message: 'Your Post have been created.',
      duration: 2000
    });
    toast.present();
  }
//Fail Toast
  async presentToastFail() {
    const toast = await this.toastController.create({
      message: 'We have a error, check your fields.',
      duration: 2000
    });
    toast.present();
  }
  //validate form
  validate(){
   
    if(this.postInput.title == "" || this.postInput.contents == "" || this.postInput.rate == ""
    || this.postInput.idAdministrator == 0 || this.postInput.idCategory == 0){
      return false;
    }
    else{return true;}
  }
//add new post
  add(){
    
   const categoParse : any = this.postInput.idCategory;
    const idCategory: number = parseInt(categoParse);
    this.postInput.idCategory = idCategory;

    let success = this.validate();
    if(success){
      this.post = this.postInput;
      this.postService.add(this.post).subscribe(()=> {
        this.presentToastOk();
        this.router.navigateByUrl('/app/tabs/posts');
      }, error=> {
        console.error("Add Error >>>", error);
        this.presentToastFail();
      });
    }
    else{this.presentToastFail();}
  }


}
