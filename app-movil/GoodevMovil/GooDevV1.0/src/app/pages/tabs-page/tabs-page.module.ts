import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs-page';
import { TabsPageRoutingModule } from './tabs-page-routing.module';

import { AboutModule } from '../about/about.module';
import { MapModule } from '../map/map.module';
import { CategoryModule } from '../category/category.module';
import { SessionDetailModule } from '../session-detail/session-detail.module';
import { PostDetailModule } from '../post-detail/post-detail.module';
import { PostListModule } from '../post-list/post-list.module';

@NgModule({
  imports: [
    AboutModule,
    CommonModule,
    IonicModule,
    MapModule,
    CategoryModule,
    SessionDetailModule,
    PostDetailModule,
    PostListModule,
    TabsPageRoutingModule
  ],
  declarations: [
    TabsPage,
  ]
})
export class TabsModule { }
