import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from '../../providers/user-data';
import { ToastController } from '@ionic/angular';
import { UserOptions } from '../../interfaces/user-options';
import { PostsService } from '../../services/posts.service'
import { Administrator } from '../../models/admin'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  styleUrls: ['./login.scss'],
})
export class LoginPage {
  login: UserOptions = { username: '', password: '' };
  submitted = false;
  admin : Array<Administrator>;
  constructor(
    public userData: UserData,
    public router: Router,
    public toastController: ToastController,
    private postsService: PostsService
  ) { }

  onLogin(form: NgForm) {
    this.submitted = true;
    this.validateLogin();

  }

  onSignup() {
    this.router.navigateByUrl('/signup');
  }

  validateLogin(){
    this.postsService.Login(this.login.username,this.login.password).subscribe(()=>{
      this.presentToastOk();
      this.router.navigateByUrl('/app/tabs/category');
    },error =>{
      this.presentToastFail();
      console.error("login error");});
  }
//OK Toast
async presentToastOk() {
  const toast = await this.toastController.create({
    message: 'You have been logged.',
    duration: 2000
  });
  toast.present();
}
//Fail Toast
async presentToastFail() {
  const toast = await this.toastController.create({
    message: 'We have a error, please check your fields.',
    duration: 2000
  });
  toast.present();
}
}
