export class Administrator {
    id: number;
    name: string;
    password: string;
    email: string;
}