![](desing/logos/gooDev3.png)
# :monkey: Here we have the differents uml of the app
## ER-Diagram
![](ERDiagram.drawio.png)
## System Context diagram
![](context_system.drawio.png)
## Containers Diagram
![](containers_diagram.drawio.png)
## Components driagram
![](components_diagram.drawio.png)
## Classes Diagram
![](classes_diagram.drawio.png)
