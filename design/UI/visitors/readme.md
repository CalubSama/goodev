![](desing/logos/gooDev3.png)
# :monkey: These are the UI views for the user visitor or public 
## view of index
![](index.drawio.png)
## view of post
![](post.drawio.png)
## view of contact
![](contact.drawio.png)
## view of categories
![](categories.drawio.png)
## view of login
![](login.drawio.png)
## view of register
![](register.drawio.png)
## view of profile
![](profile.drawio.png)

